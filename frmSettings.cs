﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vMixTitler
{
    public partial class frmSettings : Form
    {
        public frmSettings()
        {
            InitializeComponent();
            txtHost.Text = Program.vMixController.Host;
            txtPort.Text = Program.vMixController.Port.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Program.vMixController.Host = txtHost.Text;
            Program.vMixController.Port = Convert.ToInt16(txtPort.Text);
            Program.vMixController.CheckStatus();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
