﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vMixTitler
{
    static class Program
    {
        public static VMixController vMixController;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            vMixController = new VMixController();
            vMixController.Host = "127.0.0.1";
            vMixController.Port = 8088;

            vMixController.CheckStatus();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }

        
    }
}
