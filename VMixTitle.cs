﻿using System.Collections.Generic;
namespace vMixTitler
{
    class VMixTitle
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public int Number { get; set; }
        public List<string> TextItems { get; set; }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
