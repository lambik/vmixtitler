﻿namespace vMixTitler
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.pnlTitles = new System.Windows.Forms.Panel();
            this.btnShowTitle1 = new System.Windows.Forms.Button();
            this.txtDescription1 = new System.Windows.Forms.TextBox();
            this.lblItem1String2 = new System.Windows.Forms.Label();
            this.lblItem1String1 = new System.Windows.Forms.Label();
            this.txtTitle1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTitles = new System.Windows.Forms.ComboBox();
            this.lblNoTitlesFound = new System.Windows.Forms.Label();
            this.radOnline = new System.Windows.Forms.RadioButton();
            this.pnlTitles.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Fade";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.Location = new System.Drawing.Point(197, 13);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(75, 23);
            this.btnSettings.TabIndex = 1;
            this.btnSettings.Text = "Settings";
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // pnlTitles
            // 
            this.pnlTitles.AutoScroll = true;
            this.pnlTitles.Controls.Add(this.btnShowTitle1);
            this.pnlTitles.Controls.Add(this.txtDescription1);
            this.pnlTitles.Controls.Add(this.lblItem1String2);
            this.pnlTitles.Controls.Add(this.lblItem1String1);
            this.pnlTitles.Controls.Add(this.txtTitle1);
            this.pnlTitles.Location = new System.Drawing.Point(12, 135);
            this.pnlTitles.Name = "pnlTitles";
            this.pnlTitles.Size = new System.Drawing.Size(260, 114);
            this.pnlTitles.TabIndex = 2;
            // 
            // btnShowTitle1
            // 
            this.btnShowTitle1.Location = new System.Drawing.Point(146, 53);
            this.btnShowTitle1.Name = "btnShowTitle1";
            this.btnShowTitle1.Size = new System.Drawing.Size(111, 23);
            this.btnShowTitle1.TabIndex = 4;
            this.btnShowTitle1.Tag = "1";
            this.btnShowTitle1.Text = "Show this title";
            this.btnShowTitle1.UseVisualStyleBackColor = true;
            this.btnShowTitle1.Click += new System.EventHandler(this.btnShowTitle_Click);
            // 
            // txtDescription1
            // 
            this.txtDescription1.Location = new System.Drawing.Point(72, 27);
            this.txtDescription1.Name = "txtDescription1";
            this.txtDescription1.Size = new System.Drawing.Size(185, 20);
            this.txtDescription1.TabIndex = 3;
            // 
            // lblItem1String2
            // 
            this.lblItem1String2.AutoSize = true;
            this.lblItem1String2.Location = new System.Drawing.Point(3, 30);
            this.lblItem1String2.Name = "lblItem1String2";
            this.lblItem1String2.Size = new System.Drawing.Size(37, 13);
            this.lblItem1String2.TabIndex = 2;
            this.lblItem1String2.Text = "Boogy";
            // 
            // lblItem1String1
            // 
            this.lblItem1String1.AutoSize = true;
            this.lblItem1String1.Location = new System.Drawing.Point(3, 7);
            this.lblItem1String1.Name = "lblItem1String1";
            this.lblItem1String1.Size = new System.Drawing.Size(32, 13);
            this.lblItem1String1.TabIndex = 1;
            this.lblItem1String1.Text = "Oogy";
            // 
            // txtTitle1
            // 
            this.txtTitle1.Location = new System.Drawing.Point(72, 4);
            this.txtTitle1.Name = "txtTitle1";
            this.txtTitle1.Size = new System.Drawing.Size(185, 20);
            this.txtTitle1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Title to send values to:";
            // 
            // cmbTitles
            // 
            this.cmbTitles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTitles.FormattingEnabled = true;
            this.cmbTitles.Location = new System.Drawing.Point(131, 47);
            this.cmbTitles.Name = "cmbTitles";
            this.cmbTitles.Size = new System.Drawing.Size(138, 21);
            this.cmbTitles.TabIndex = 4;
            this.cmbTitles.SelectedIndexChanged += new System.EventHandler(this.cmbTitles_SelectedIndexChanged);
            // 
            // lblNoTitlesFound
            // 
            this.lblNoTitlesFound.AutoSize = true;
            this.lblNoTitlesFound.ForeColor = System.Drawing.Color.Red;
            this.lblNoTitlesFound.Location = new System.Drawing.Point(12, 71);
            this.lblNoTitlesFound.Name = "lblNoTitlesFound";
            this.lblNoTitlesFound.Size = new System.Drawing.Size(241, 13);
            this.lblNoTitlesFound.TabIndex = 5;
            this.lblNoTitlesFound.Text = "Warning: no titles found in vMix! Add at least one!";
            // 
            // radOnline
            // 
            this.radOnline.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radOnline.Location = new System.Drawing.Point(170, 14);
            this.radOnline.Name = "radOnline";
            this.radOnline.Size = new System.Drawing.Size(20, 20);
            this.radOnline.TabIndex = 6;
            this.radOnline.UseVisualStyleBackColor = true;
            this.radOnline.Visible = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.radOnline);
            this.Controls.Add(this.lblNoTitlesFound);
            this.Controls.Add(this.cmbTitles);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlTitles);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.button1);
            this.Name = "frmMain";
            this.Text = "Set titles";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmMain_Paint);
            this.pnlTitles.ResumeLayout(false);
            this.pnlTitles.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Panel pnlTitles;
        private System.Windows.Forms.TextBox txtDescription1;
        private System.Windows.Forms.Label lblItem1String2;
        private System.Windows.Forms.Label lblItem1String1;
        private System.Windows.Forms.TextBox txtTitle1;
        private System.Windows.Forms.Button btnShowTitle1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTitles;
        private System.Windows.Forms.Label lblNoTitlesFound;
        private System.Windows.Forms.RadioButton radOnline;
    }
}

