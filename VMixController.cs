﻿using System;
using System.Text;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace vMixTitler
{
    class VMixController
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool Online { get; set; }
        public List<VMixTitle> Titles { get; set; }

        public void CheckStatus()
        {
            this.Online = false;

            string status = this.GetStatus();
            if (!status.Equals(""))
            {
                this.Online = true;

                this.Titles = new List<VMixTitle>();

                // parse XML
                XDocument doc = XDocument.Parse(status);

                // filter <input type="Xaml"> elements
                var filteredTitles = from x in doc.Descendants("inputs").Descendants("input")
                                     where x.Attribute("type").Value == "Xaml"
                                     select x;

                foreach (var item in filteredTitles)
                {
                    VMixTitle title = new VMixTitle();
                    title.Name = item.Attribute("title").Value;
                    title.Number = Convert.ToInt32(item.Attribute("number").Value);
                    title.Key = item.Attribute("key").Value;
                    title.TextItems = new List<string>();

                    foreach (var itemText in item.Descendants("text"))
                    {
                        title.TextItems.Add(itemText.Attribute("name").Value);
                    }
                    this.Titles.Add(title);
                }
            }
        }

        public string GetStatus()
        {
            try
            {
                return this.SendCommand(null, null, null, null, null, null);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string SendCommand(string function, string duration, string input, string selectedName, string selectedIndex, string value)
        {
            string returnText = "";

            UriBuilder ub = new UriBuilder("http", this.Host, this.Port, "api/");

            Uri url = ub.Uri
                .AddQuery("Function", function)
                .AddQuery("Duration", duration)
                .AddQuery("Input", input)
                .AddQuery("SelectedName", selectedName)
                .AddQuery("SelectedIndex", selectedIndex)
                .AddQuery("Value", value)
            ;

            WebRequest request = HttpWebRequest.Create(url);
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception("Error occurred when sending request!");
                }
                StreamReader reader = new StreamReader(response.GetResponseStream());
                returnText = reader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new Exception("Could not find vMix, is it running?");
            }

            return returnText;
        }

        public void SendCommand(string function, int duration, int input, string selectedName, int selectedIndex, string value)
        {
            this.SendCommand(function, duration.ToString(), input.ToString(), selectedName, selectedIndex.ToString(), value);
        }

        public void SendCommandWithDuration(string function, int duration)
        {
            this.SendCommandWithDuration(function, duration, null);
        }

        public void SendCommandWithDuration(string function, int duration, string input)
        {
            this.SendCommand(function, duration.ToString(), input, null, null, null);
        }

        public void SendCommandWithDuration(string function, int duration, int input)
        {
            this.SendCommand(function, duration.ToString(), input.ToString(), null, null, null);
        }

        public void SendCommandWithName(string function, string selectedName, string value)
        {
            this.SendCommandWithName(function, selectedName, value, null);
        }

        public void SendCommandWithName(string function, string selectedName, string value, int input)
        {
            this.SendCommandWithName(function, selectedName, value, input.ToString());
        }

        public void SendCommandWithName(string function, string selectedName, string value, string input)
        {
            this.SendCommand(function, null, input, selectedName, null, value);
        }

        public void SendCommandWithIndex(string function, string selectedIndex, string value)
        {
            this.SendCommandWithIndex(function, selectedIndex, value, null);
        }

        public void SendCommandWithIndex(string function, string selectedIndex, string value, int input)
        {
            this.SendCommandWithIndex(function, selectedIndex, value, input.ToString());
        }

        public void SendCommandWithIndex(string function, string selectedIndex, string value, string input)
        {
            this.SendCommand(function, null, input, null, selectedIndex, value);
        }

        public void SendCommandWithInput(string function, int input)
        {
            this.SendCommandWithInput(function, input.ToString());
        }

        public void SendCommandWithInput(string function, string input)
        {
            this.SendCommand(function, null, input, null, null, null);
        }
    }
}
