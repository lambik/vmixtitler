﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vMixTitler
{
    public partial class frmMain : Form
    {
        private VMixTitle CurrentTitle { get; set; }
        private int ShowingTitleIndex { get; set; }

        public frmMain()
        {
            InitializeComponent();

            this.ShowingTitleIndex = -1;

            if (Program.vMixController.Online)
            {
                List<VMixTitle> titles = Program.vMixController.Titles;
                cmbTitles.Items.Clear();
                if (titles.Count > 0)
                {
                    lblNoTitlesFound.Hide();
                    cmbTitles.Items.AddRange(titles.ToArray());
                    cmbTitles.SelectedIndex = 0;
                    // load the first title
                    cmbTitles_SelectedIndexChanged(cmbTitles, null);
                }
                else
                {
                    lblNoTitlesFound.Show();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // send command to vMix
            string function = "Fade";
            int duration = 1000;

            try
            {
                Program.vMixController.SendCommandWithDuration(function, duration);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error!", MessageBoxButtons.OK);
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            frmSettings settings = new frmSettings();
            settings.ShowDialog();
            Invalidate();
        }

        private void btnShowTitle_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int titleIndex = Convert.ToInt16(btn.Tag);
            
            try
            {
                Program.vMixController.SendCommandWithName("SetText", this.CurrentTitle.TextItems[0], txtTitle1.Text, this.CurrentTitle.Number);
                Program.vMixController.SendCommandWithName("SetText", this.CurrentTitle.TextItems[1], txtDescription1.Text, this.CurrentTitle.Number);
                
                // if we pressed the show button for the title text last shown, hide the title overlay
                if (this.ShowingTitleIndex == titleIndex)
                {
                    Program.vMixController.SendCommandWithInput("OverlayInput1Out", this.CurrentTitle.Number);
                    this.ShowingTitleIndex = -1;
                }
                // else if we are not showing anything currently, start the overlay
                else if (this.ShowingTitleIndex == -1)
                {
                    Program.vMixController.SendCommandWithInput("OverlayInput1In", this.CurrentTitle.Number);
                    this.ShowingTitleIndex = titleIndex;
                }
                else
                {
                    // else we are showing different text, but the overlay is already on, so no need to send any command
                    this.ShowingTitleIndex = titleIndex;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error!", MessageBoxButtons.OK);
            }
        }

        private void frmMain_Paint(object sender, PaintEventArgs e)
        {
            // draw the online/offline status of vMix as a circle

            Rectangle rectangle = new Rectangle(radOnline.Location.X, radOnline.Location.Y, radOnline.Width, radOnline.Height);

            Color color = Color.Red;
            if (Program.vMixController.Online)
            {
                color = Color.Green;
            }
            e.Graphics.FillEllipse(new SolidBrush(color), rectangle);
        }

        private void cmbTitles_SelectedIndexChanged(object sender, EventArgs e)
        {
            VMixTitle title = (VMixTitle)((ComboBox)sender).SelectedItem;
            this.CurrentTitle = title;
            lblItem1String1.Text = title.TextItems[0] + ":";
            lblItem1String2.Text = title.TextItems[1] + ":";
        }

    }
}
